var AWS = require('aws-sdk');

exports.handler = function(event, context) {
   console.log(event);
   console.log(event.Records[0].dynamodb.NewImage.payload)
   console.log(event.Records[0].dynamodb.NewImage.payload.M.data.M.data.S)

   var cw = new AWS.CloudWatch({apiVersion: '2010-08-01'});

   // Create parameters JSON for putMetricData
   var params = {
      MetricData: [
         {
            MetricName: 'TEMPERATURE',
            Dimensions: [
               {
                  Name: 'SOIL_TEMP',
                  Value: 'CELCIUS'
               },
            ],
            Unit: 'None',
            Value: parseFloat(event.Records[0].dynamodb.NewImage.payload.M.data.M.data.S)
         },
      ],
      Namespace: 'GREENHOUSE/SENSORS'
   };

   cw.putMetricData(params, function(err, data) {
      if (err) {
         console.log("Error", err);
      } else {
         console.log("Success", JSON.stringify(data));
      }
   });

};
